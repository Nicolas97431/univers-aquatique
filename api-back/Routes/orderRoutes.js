//ma clef de test stripe
const stripe = require("stripe")(
  "sk_test_51LoWZjBQwgcwEFPsvJRPQsmwbSesyPskhP1TYRgsQIJZWLfET0aygSeqY9wB0kSaqgVI0n08qcYQfneyH2CGZq8H00JNLqp2iN"
);
const withAuth = require("../withAuth");

module.exports = (app, db) => {
  const orderModel = require("../models/Ordermodel")(db);
  const articleModel = require("../models/ArticleModel")(db);
  const userModel = require("../models/UserModel")(db);

  //Route d'enregistrement d'une commande
  app.post("/order/save", withAuth, async (req, res, next) => {
    console.log("Données reçues par la route /order/save :", req.body);

    let totalPrice = 0;
    let totalQuantity = 0;
    req.body.basket.forEach((product) => {
      totalQuantity += product.quantityInCart;
    });

    const orderInfos = await orderModel.saveOneOrder(
      req.body.user_id,
      totalPrice,
      totalQuantity
    );
    //console.log(orderInfos)
    if (orderInfos.code) {
      console.log("les infos  de order", orderInfos);
      res.json({
        status: 500,
        msg: "Echec de l'enregistrement de la commande",
      });
    } else {
      //insertId = id qu'il viens d'inserer dans la bdd
      const orderId = orderInfos.insertId;
      //boucle pour recup de tous les articles dans le panier
      req.body.basket.forEach(async (products) => {
        const product = await articleModel.getOneProduct(products.id);
        if (product.code) {
          res.json({
            status: 500,
            msg: "Echec de l'enregistrement de la commande",
          });
        } else {
          products.safePrice = parseFloat(product[0].price);
          console.log("orderId:", orderId);
          console.log("product:", product);
          const detail = await orderModel.saveOneOrderLine(orderId, products);
          console.log("les infos order_line", detail);
          if (detail.code) {
            res.json({
              status: 500,
              msg: "Echec de l'enregistrement de la commande",
            });
          } else {
            // calcul du montant de la commande
            totalPrice += parseInt(products.quantityInCart) * products.price;
            //mise a jours du montant total
            const update = await orderModel.updateTotalCommande(
              orderId,
              totalPrice
            );
          }
        }
      });
      // tout est OK on retourne l'ID de la commande enregistrée
      res.json({ status: 200, orderId: orderId });
    }
  });

  //---------------------------------------------------------------------------------------------------------------
  //                                    paiement
  //---------------------------------------------------------------------------------------------------------------

  //Route de gestion de paiement
  app.post("/order/payment", withAuth, async (req, res, next) => {
    console.log("Payment data received:", req.body);
    console.log("Order ID for payment:", req.body.orderId);
    const order = await orderModel.getOneOrder(req.body.orderId);
    if (order.code) {
      res.json({ status: 500, msg: "Le paiement ne peut être vérifié" });
    } else {
      // suivi du paiement
      const payementIntent = await stripe.paymentIntents.create({
        amount: order[0].totalPrice * 100,
        currency: "eur",
        metadata: { integration_check: "accept_a_payement" },
        receipt_email: req.body.email,
      });
      res.json({ status: 200, client_secret: payementIntent["client_secret"] });
    }
  });

  //-------------------------------------------------------------------------------------------------------------------

  //Route de modif du status de la commande

  app.put("/order/validate", withAuth, async (req, res, next) => {
    console.log(
      "Updating order status for order ID:",
      req.body.orderId,
      "with status ID:",
      req.body.id_order_status
    );

    try {
      const validate = await orderModel.updateOrderStatus(
        req.body.id_order_status,
        req.body.orderId
      );

      if (validate) {
        res.json({ status: 200, msg: "Status de commande mis à jour" });
      } else {
        res.json({ status: 500, msg: "Une erreur est survenue" });
      }
    } catch (err) {
      console.error(err);
      res.status(500).json({ status: 500, msg: "Une erreur est survenue" });
    }
  });

  //Route de recuperation de toutes les commandes
  app.get("/order/all", withAuth, async (req, res, next) => {
    const orders = await orderModel.getAllOrders();
    if (orders.code) {
      res.josn({ status: 500, msg: "Une erreur est survenue" });
    } else {
      res.json({ status: 200, result: orders });
    }
  });

  // Reucuperation d'une commande complete
  app.get("/order/getOneOrder/:id", withAuth, async (req, res, next) => {
    console.log("Fetching order with ID:", req.params.id);
    const order = await orderModel.getOneOrder(req.params.id);
    console.log("Order fetched successfully:", order);
    if (order.code) {
      console.log("Error fetching order:", order);
      res.json({ status: 500, msg: "Une erreur est survenue" });
    } else {
      console.log("Fetching user with ID:", order[0].id_user);
      const user = await userModel.getUserById(order[0].id_user);
      console.log("User fetched successfully:", user);
      if (user.code) {
        console.log("Error fetching user:", user);
        res.json({ status: 500, msg: "Une erreur est survenue" });
      } else {
        const myUser = {
          firstname: user[0].firstname,
          lastname: user[0].lastname,
          email: user[0].email,
          phone: user[0].phone,
        };
        //recuperation des order_line
        console.log("Fetching order lines for order ID:", req.params.id);
        const detail = await orderModel.getAllOrdersLine(req.params.id);
        console.log("Order lines fetched successfully:", detail);
        if (detail.code) {
          console.log("Error fetching order lines:", detail);
          res.json({ status: 500, msg: "Une erreur est survenue" });
        } else {
          //console.log("An error occurred:", error);
          res.json({
            status: 200,
            order: order[0],
            user: myUser,
            order_line: detail,
          });
        }
      }
    }
  });
};
