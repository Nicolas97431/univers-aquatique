//pour supprimer les images locales
const fs = require("fs");
const upload = require("../multerConfig");
const path = require("path");
const withAuth = require("../withAuth");
const withAuthAdmin = require("../withAuthAdmin");
const { clearScreenDown } = require("readline");
//const articleModel = require("../models/ArticleModel");
//const withAuthPrepa = require("../withAuthPrepa");

module.exports = (app, db) => {
  const articleModel = require("../models/ArticleModel")(db);

  //Route de recuperation de tous les articles
  app.get("/article/all", async (req, res, next) => {
    const products = await articleModel.getAllProducts();
    if (products.code) {
      res.json({
        status: 500,
        msg: "Accés a la liste des produits impossible",
      });
    } else {
      res.json({ status: 200, result: products });
    }
  });

  //Route de recuperation d'un seul article
  app.get("/article/one/:id", async (req, res, next) => {
    const product = await articleModel.getOneProduct(req.params.id);
    if (product.code) {
      res.json({
        status: 500,
        msg: "Erreur, impossible de récupérer cet l'article",
      });
    } else {
      res.json({ status: 200, result: product[0] });
    }
  });

  //Route d'ajout d'un article
  app.post("/article/save", async (req, res, next) => {
    // j'ai supprimer temporairement a remetre avant async withAuthAdmin,
    const product = await articleModel.saveOneProduct(req);
    if (product.code) {
      res.json({ status: 500, msg: "Erreur: article non enregistré" });
    } else {
      res.json({ status: 200, msg: "L'article a bien été ajouter" });
    }
  });

  //Route pour l'ajout d'une immage dans la base
  // app.post("/article/img", async (req, res, next) => {
  //   // console.log("Requête de téléchargement d'image reçue :", req.body);
  //   // console.log("Fichiers téléchargés :", req.files);
  //   //si pas envoyer ou si objet vide de cette propriete
  //   if (!req.files || Object.keys(req.files).length === 0) {
  //     res.json({
  //       status: 400,
  //       msg: "Ce produit n'a pas de photo ou celle-ci est innaccessible",
  //     });
  //   } else {
  //     //stock de l'image dans dossier Public/images-produits
  //     req.files.image.mv(
  //       `public/images-produits/${req.files.image.name}`,
  //       (err) => {
  //         if (err) {
  //           res.json({ status: 500, msg: "Erreur: image non enregistrée" });
  //         } else {
  //           console.log("verification name", req.files.image.name);
  //           //image enregistrée on retourne son nom vers le front
  //           res.json({
  //             status: 200,
  //             msg: "L'image a bien été enregistrée",
  //             url: req.files.image.name,
  //           });
  //         }
  //       }
  //     );
  //   }
  // });

  // Route pour l'ajout d'une image dans la base
  app.post("/article/img", async (req, res, next) => {
    if (!req.files || Object.keys(req.files).length === 0) {
      return res.json({
        status: 400,
        msg: "Ce produit n'a pas de photo ou celle-ci est inaccessible",
      });
    }

    const newImageName = req.header("New-Image-Name"); // Récupération du nouveau nom de l'image depuis l'en-tête de la requête

    req.files.image.mv(`public/images-produits/${newImageName}`, (err) => {
      if (err) {
        return res.json({ status: 500, msg: "Erreur: image non enregistrée" });
      } else {
        // console.log("Nouveau nom de l'image :", newImageName);
        // Image enregistrée, retournez le nouveau nom vers le front
        return res.json({
          status: 200,
          msg: "L'image a bien été enregistrée",
          url: newImageName,
        });
      }
    });
  });

  //Route de  modification d'un article
  app.put(
    "/article/update/:id",
    //withAuthAdmin,
    withAuth,
    async (req, res, next) => {
      console.log(
        "Identifiant du produit reçu depuis le front-end :",
        req.params.id
      );
      const product = await articleModel.updateOneProduct(req, req.params.id);
      if (product.code) {
        console.error("Erreur lors de la modification de l'article :", product);
        res.json({ status: 500, msg: "Erreur: modification impossible" });
      } else {
        console.log("L'article a été modifié avec succès");
        res.json({ status: 200, msg: "L'article a été modifié avec succés" });
      }
    }
  );

  //Route de suppression d'un article
  app.delete("/article/delete/:id", withAuth, async (req, res, next) => {
    //avant suppression on stocke en memoire les infos produit pour pouvoir supprimer l'image
    const product = await articleModel.getOneProduct(req.params.id);
    console.log("Produit récupéré :", product);
    if (product.code) {
      res.json({ status: 500, msg: "Une erreur est survenue" });
    } else {
      const deleteProduc = await articleModel.deleteOneProduct(req.params.id);
      if (deleteProduc.code) {
        res.json({
          status: 500,
          msg: "Erreur:  suppression Impossible article en commade  ",
        });
      } else {
        // suppression -> ok, il faut supprimer l'image
        if (product[0].img !== "no-pict.jpg") {
          fs.unlink(`public/images-produits/${product[0].img}`, (err) => {
            if (err) {
              console.error("Erreur lors de la suppression de l'image :", err);
              res.json({
                status: 500,
                msg: "Erreur: impossible de supprimer l'image",
              });
            }
            console.log(
              "L'image a été supprimée avec succès :",
              product[0].img
            );
          });

          res.json({ status: 200, msg: "Article supprimer avec succés" });
        } else {
          console.log("Pas d'image à supprimer pour ce produit");
          console.error("Erreur lors de la suppression de l'article :", err);
          res.json({ status: 200, msg: "Article supprimer avec succés" });
        }
      }
    }
  });

  //---------------------------------------------------------------------------------------------------------------
  //----------------------Images table picture ----------------------------------------------

  // Route pour l'ajout d'une image dans la base
  app.post("/article/picture", async (req, res, next) => {
    try {
      console.log("req:", req.files.image);
      // Récupérer les données de l'image depuis la requête
      const pictureData = {
        name: req.body.name,
        alt: req.body.alt,
        id_product: req.body.id_product,
      };
      console.log("Données de l'image reçues :", pictureData);
      // Appeler une méthode du modèle pour enregistrer les données de l'image dans la base de données
      const result = await articleModel.savePictureData(pictureData);

      // Envoyer une réponse indiquant que l'ajout de l'image a réussi
      return res.status(200).json({ msg: "L'image a été ajoutée avec succès" });
    } catch (err) {
      // Gérer les erreurs
      console.error("Erreur lors de l'ajout de l'image :", err);
      return res
        .status(500)
        .json({ msg: "Une erreur est survenue lors de l'ajout de l'image" });
    }
  });

  // Route pour récupérer les images associées à un produit spécifique
  app.get("/article/images/:productId", async (req, res, next) => {
    try {
      const productId = req.params.productId;
      const images = await articleModel.getProductImages(productId);
      res.json({ status: 200, images: images });
    } catch (err) {
      console.error("Erreur lors de la récupération des images :", err);
      res.status(500).json({
        msg: "Une erreur est survenue lors de la récupération des images",
      });
    }
  });
};
