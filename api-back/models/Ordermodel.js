module.exports = (_db) => {
  db = _db;
  return OrderModel;
};
//-const formatDate = require("./FormatDate");
class OrderModel {
  // Ajout d'une commande avec option de payment
  static saveOneOrder(userId, totalPrice, totalQuantity, orderDate, payment) {
    console.log(
      "Data received for saving order:",
      userId,
      totalPrice,
      totalQuantity
    );
    //const orderDate = formatDate(new Date());
    return db
      .query(
        //"INSERT INTO orders (id_user, total_price, total_quantity, order_date, id_order_status, id_shop, payment) VALUES (?,?,?,NOW(),?,?,'not payed')",
        //"INSERT INTO orders (id_user, total_price, total_quantity, order_date, id_order_status, id_shop, payment) VALUES (?,?,?,NOW(),?,?,'not payed')",
        //[userId, totalPrice, totalQuantity, 1, 1]
        "INSERT INTO orders (id_user, total_price, total_quantity, order_date, id_order_status, id_shop, payment) VALUES (?,?,?,NOW(),'1','1','not payed')",
        [userId, totalPrice, totalQuantity, orderDate, payment]
      )
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err;
      });
  }

  // Ajout d'une ligne de commande (qui sera appeler en boucle en fonction du nombre d'articles)
  static saveOneOrderLine(orderId, product) {
    const total = parseInt(product.quantityInCart) * product.price;
    return db
      .query(
        //   "INSERT INTO order_line (id_product, quantity, unit_price, id_order) VALUES (?,?,?,?)",
        //   [orderId, product.id, product.quantityInCart, Price]
        // )
        // "INSERT INTO order_line (id_product, quantity, unit_price, id_order) VALUES (?,?,?,?)",
        // [product.id, product.quantityInCart, product.safePrice, orderId]
        "INSERT INTO order_line (id_product, quantity, unit_price, id_order,total ) SELECT ?, ?, ?, ?, (SELECT price FROM product WHERE id = ?)",
        [product.id, product.quantityInCart, product.price, orderId, product.id]
      )
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err;
      });
  }

  //Modif montant total
  static updateTotalCommande(orderId, totalPrice) {
    return db
      .query("UPDATE orders SET total_price =? WHERE id =?", [
        totalPrice,
        orderId,
      ])
      .then((res) => {
        console.log("Order fetched successfully:", res);
        return res;
      })
      .catch((err) => {
        console.error("Error fetching order:", err);
        return err;
      });
  }

  //Recup commande par ID
  static getOneOrder(id) {
    console.log("Fetching order with ID:", id);
    return db
      .query("SELECT * FROM orders WHERE id = ?", [id])
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err;
      });
  }

  //modif Status de la commande
  static updateOrderStatus(id_order_status, orderId) {
    console.log(
      "Updating order status for order ID:",
      orderId,
      "withid_order_status:",
      id_order_status,
      "user "
    );
    return db
      .query(
        //"UPDATE `orders` SET `id_order_status` = '3' WHERE `orders`.`id` = 1;",
        "UPDATE orders SET id_order_status = ? WHERE id= ?",
        //"UPDATE orders SET id_order_status = (SELECT id FROM order_status WHERE type= ?) WHERE id= ?",
        [id_order_status, orderId]
      )
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err;
      });
  }

  //recup de toutes les commandes
  static getAllOrders() {
    return db
      .query("SELECT * FROM orders")
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err;
      });
  }

  // recup des lignes d'une commande
  static getAllOrdersLine(orderId) {
    return db
      .query(
        "SELECT order_line.id, order_line.quantity, price, name, img, content FROM order_line INNER JOIN product ON product.id = order_line.id_product WHERE id_order = ?",
        // "SELECT order_line.id, order_line.quantity, price, name  FROM order_line INNER JOIN product ON product.id = order_line.id_product WHERE id_order =?",
        [orderId]
      )
      .then((res) => {
        return res;
      })
      .catch((err) => {
        return err;
      });
  }
}
